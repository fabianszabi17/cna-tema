package service;

import io.grpc.stub.StreamObserver;
import proto.ClientGrpc;
import proto.HomeworkCNP;

public class ClientImpl extends ClientGrpc.ClientImplBase {

    /*
     * We observe here that some words have an "@", this are Annotations. Annotations are used to provide supplement
     * information about a program. We can autogenerate this functions, in Intellij we can use the shortcut ctrl + O to
     * do this.
     * */
    @Override
    public void sayInfo(HomeworkCNP.NameCNP request, StreamObserver<HomeworkCNP.ServerReply> responseObserver) {
        HomeworkCNP.ServerReply reply = HomeworkCNP.ServerReply.newBuilder().setMessage("Hello " + request.getName() + "").build();
        /* We can call multiple times onNext function if we have multiple replies, ex. in next commits */
         responseObserver.onNext(reply);
        /* We use the response observer's onCompleted method to specify that we've finished dealing with the RPC */
        responseObserver.onCompleted();

        //HomeworkCNP.ServerReply serverReply = HomeworkCNP.ServerReply.newBuilder().setSex(("Your sex is") + request.getGender() + "").build();
        //responseObserver.onNext(serverReply);

        //responseObserver.onCompleted();
    }

    @Override
    public void sayInfoAgain(HomeworkCNP.NameCNP request, StreamObserver<HomeworkCNP.ServerReply> responseObserver) {
        // HomeworkCNP.ServerReply reply = HomeworkCNP.ServerReply.newBuilder().setMessage("Hello again " + request.getName()).build();

        HomeworkCNP.ServerReply serverReply = HomeworkCNP.ServerReply.newBuilder().setSex(("Your sex is") + request.getGender() + "").build();
        responseObserver.onNext(serverReply);

        //responseObserver.onNext(reply);
        responseObserver.onCompleted();
    }

    @Override
    public void return_(HomeworkCNP.Cnp request, StreamObserver<HomeworkCNP.CnpReply> responseObserver) {
        String string = request.getCnp();
        int bornYear = Integer.parseInt(string.substring(1, 3));
        //System.out.println(bornYear);
        int age;

        char Gender = string.charAt(1);
        if ((string.charAt(0) == '5')) {
            age = 2021 - 2000-bornYear;
            HomeworkCNP.CnpReply reply1 = HomeworkCNP.CnpReply.newBuilder().setMessage("Your gender is MAN " + "Age " + age).build();
            responseObserver.onNext(reply1);
            responseObserver.onCompleted();
        }
        else {
            if (string.charAt(0) == '6') {
                age = 2021 - 2000 - bornYear;
                HomeworkCNP.CnpReply reply2 = HomeworkCNP.CnpReply.newBuilder().setMessage("Your gender is WOMAN " + "Age " + age).build();
                responseObserver.onNext(reply2);
                responseObserver.onCompleted();
            }
            else {
                if (string.charAt(0) == '1') {
                    age = 2021 - 1900 - bornYear;
                    HomeworkCNP.CnpReply reply3 = HomeworkCNP.CnpReply.newBuilder().setMessage("Your gender is MEN " + "Age " + age).build();
                    responseObserver.onNext(reply3);
                    responseObserver.onCompleted();
                }
                else {
                    if (string.charAt(0) == '2') {
                        age = 2021 - 1900 - bornYear;
                        HomeworkCNP.CnpReply reply4 = HomeworkCNP.CnpReply.newBuilder().setMessage("Your gender is WOMAN " + "Age " + age).build();
                        responseObserver.onNext(reply4);
                        responseObserver.onCompleted();
                    }
                }
            }
        }
    }


}
