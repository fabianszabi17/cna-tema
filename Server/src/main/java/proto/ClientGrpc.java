package proto;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 * <pre>
 * The greeting service definition. 
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: HomeworkCNP.proto")
public final class ClientGrpc {

  private ClientGrpc() {}

  public static final String SERVICE_NAME = "Client";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<proto.HomeworkCNP.NameCNP,
      proto.HomeworkCNP.ServerReply> getSayInfoMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SayInfo",
      requestType = proto.HomeworkCNP.NameCNP.class,
      responseType = proto.HomeworkCNP.ServerReply.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<proto.HomeworkCNP.NameCNP,
      proto.HomeworkCNP.ServerReply> getSayInfoMethod() {
    io.grpc.MethodDescriptor<proto.HomeworkCNP.NameCNP, proto.HomeworkCNP.ServerReply> getSayInfoMethod;
    if ((getSayInfoMethod = ClientGrpc.getSayInfoMethod) == null) {
      synchronized (ClientGrpc.class) {
        if ((getSayInfoMethod = ClientGrpc.getSayInfoMethod) == null) {
          ClientGrpc.getSayInfoMethod = getSayInfoMethod = 
              io.grpc.MethodDescriptor.<proto.HomeworkCNP.NameCNP, proto.HomeworkCNP.ServerReply>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "Client", "SayInfo"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  proto.HomeworkCNP.NameCNP.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  proto.HomeworkCNP.ServerReply.getDefaultInstance()))
                  .setSchemaDescriptor(new ClientMethodDescriptorSupplier("SayInfo"))
                  .build();
          }
        }
     }
     return getSayInfoMethod;
  }

  private static volatile io.grpc.MethodDescriptor<proto.HomeworkCNP.Cnp,
      proto.HomeworkCNP.CnpReply> getReturnMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Return",
      requestType = proto.HomeworkCNP.Cnp.class,
      responseType = proto.HomeworkCNP.CnpReply.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<proto.HomeworkCNP.Cnp,
      proto.HomeworkCNP.CnpReply> getReturnMethod() {
    io.grpc.MethodDescriptor<proto.HomeworkCNP.Cnp, proto.HomeworkCNP.CnpReply> getReturnMethod;
    if ((getReturnMethod = ClientGrpc.getReturnMethod) == null) {
      synchronized (ClientGrpc.class) {
        if ((getReturnMethod = ClientGrpc.getReturnMethod) == null) {
          ClientGrpc.getReturnMethod = getReturnMethod = 
              io.grpc.MethodDescriptor.<proto.HomeworkCNP.Cnp, proto.HomeworkCNP.CnpReply>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "Client", "Return"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  proto.HomeworkCNP.Cnp.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  proto.HomeworkCNP.CnpReply.getDefaultInstance()))
                  .setSchemaDescriptor(new ClientMethodDescriptorSupplier("Return"))
                  .build();
          }
        }
     }
     return getReturnMethod;
  }

  private static volatile io.grpc.MethodDescriptor<proto.HomeworkCNP.NameCNP,
      proto.HomeworkCNP.ServerReply> getSayInfoAgainMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SayInfoAgain",
      requestType = proto.HomeworkCNP.NameCNP.class,
      responseType = proto.HomeworkCNP.ServerReply.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<proto.HomeworkCNP.NameCNP,
      proto.HomeworkCNP.ServerReply> getSayInfoAgainMethod() {
    io.grpc.MethodDescriptor<proto.HomeworkCNP.NameCNP, proto.HomeworkCNP.ServerReply> getSayInfoAgainMethod;
    if ((getSayInfoAgainMethod = ClientGrpc.getSayInfoAgainMethod) == null) {
      synchronized (ClientGrpc.class) {
        if ((getSayInfoAgainMethod = ClientGrpc.getSayInfoAgainMethod) == null) {
          ClientGrpc.getSayInfoAgainMethod = getSayInfoAgainMethod = 
              io.grpc.MethodDescriptor.<proto.HomeworkCNP.NameCNP, proto.HomeworkCNP.ServerReply>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "Client", "SayInfoAgain"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  proto.HomeworkCNP.NameCNP.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  proto.HomeworkCNP.ServerReply.getDefaultInstance()))
                  .setSchemaDescriptor(new ClientMethodDescriptorSupplier("SayInfoAgain"))
                  .build();
          }
        }
     }
     return getSayInfoAgainMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static ClientStub newStub(io.grpc.Channel channel) {
    return new ClientStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static ClientBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new ClientBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static ClientFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new ClientFutureStub(channel);
  }

  /**
   * <pre>
   * The greeting service definition. 
   * </pre>
   */
  public static abstract class ClientImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     * Sends a greeting 
     * </pre>
     */
    public void sayInfo(proto.HomeworkCNP.NameCNP request,
        io.grpc.stub.StreamObserver<proto.HomeworkCNP.ServerReply> responseObserver) {
      asyncUnimplementedUnaryCall(getSayInfoMethod(), responseObserver);
    }

    /**
     */
    public void return_(proto.HomeworkCNP.Cnp request,
        io.grpc.stub.StreamObserver<proto.HomeworkCNP.CnpReply> responseObserver) {
      asyncUnimplementedUnaryCall(getReturnMethod(), responseObserver);
    }

    /**
     * <pre>
     * Sends another greeting 
     * </pre>
     */
    public void sayInfoAgain(proto.HomeworkCNP.NameCNP request,
        io.grpc.stub.StreamObserver<proto.HomeworkCNP.ServerReply> responseObserver) {
      asyncUnimplementedUnaryCall(getSayInfoAgainMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getSayInfoMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                proto.HomeworkCNP.NameCNP,
                proto.HomeworkCNP.ServerReply>(
                  this, METHODID_SAY_INFO)))
          .addMethod(
            getReturnMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                proto.HomeworkCNP.Cnp,
                proto.HomeworkCNP.CnpReply>(
                  this, METHODID_RETURN)))
          .addMethod(
            getSayInfoAgainMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                proto.HomeworkCNP.NameCNP,
                proto.HomeworkCNP.ServerReply>(
                  this, METHODID_SAY_INFO_AGAIN)))
          .build();
    }
  }

  /**
   * <pre>
   * The greeting service definition. 
   * </pre>
   */
  public static final class ClientStub extends io.grpc.stub.AbstractStub<ClientStub> {
    private ClientStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ClientStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ClientStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ClientStub(channel, callOptions);
    }

    /**
     * <pre>
     * Sends a greeting 
     * </pre>
     */
    public void sayInfo(proto.HomeworkCNP.NameCNP request,
        io.grpc.stub.StreamObserver<proto.HomeworkCNP.ServerReply> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSayInfoMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void return_(proto.HomeworkCNP.Cnp request,
        io.grpc.stub.StreamObserver<proto.HomeworkCNP.CnpReply> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getReturnMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * Sends another greeting 
     * </pre>
     */
    public void sayInfoAgain(proto.HomeworkCNP.NameCNP request,
        io.grpc.stub.StreamObserver<proto.HomeworkCNP.ServerReply> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSayInfoAgainMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * The greeting service definition. 
   * </pre>
   */
  public static final class ClientBlockingStub extends io.grpc.stub.AbstractStub<ClientBlockingStub> {
    private ClientBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ClientBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ClientBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ClientBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     * Sends a greeting 
     * </pre>
     */
    public proto.HomeworkCNP.ServerReply sayInfo(proto.HomeworkCNP.NameCNP request) {
      return blockingUnaryCall(
          getChannel(), getSayInfoMethod(), getCallOptions(), request);
    }

    /**
     */
    public proto.HomeworkCNP.CnpReply return_(proto.HomeworkCNP.Cnp request) {
      return blockingUnaryCall(
          getChannel(), getReturnMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     * Sends another greeting 
     * </pre>
     */
    public proto.HomeworkCNP.ServerReply sayInfoAgain(proto.HomeworkCNP.NameCNP request) {
      return blockingUnaryCall(
          getChannel(), getSayInfoAgainMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * The greeting service definition. 
   * </pre>
   */
  public static final class ClientFutureStub extends io.grpc.stub.AbstractStub<ClientFutureStub> {
    private ClientFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ClientFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ClientFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ClientFutureStub(channel, callOptions);
    }

    /**
     * <pre>
     * Sends a greeting 
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<proto.HomeworkCNP.ServerReply> sayInfo(
        proto.HomeworkCNP.NameCNP request) {
      return futureUnaryCall(
          getChannel().newCall(getSayInfoMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<proto.HomeworkCNP.CnpReply> return_(
        proto.HomeworkCNP.Cnp request) {
      return futureUnaryCall(
          getChannel().newCall(getReturnMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     * Sends another greeting 
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<proto.HomeworkCNP.ServerReply> sayInfoAgain(
        proto.HomeworkCNP.NameCNP request) {
      return futureUnaryCall(
          getChannel().newCall(getSayInfoAgainMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_SAY_INFO = 0;
  private static final int METHODID_RETURN = 1;
  private static final int METHODID_SAY_INFO_AGAIN = 2;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final ClientImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(ClientImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_SAY_INFO:
          serviceImpl.sayInfo((proto.HomeworkCNP.NameCNP) request,
              (io.grpc.stub.StreamObserver<proto.HomeworkCNP.ServerReply>) responseObserver);
          break;
        case METHODID_RETURN:
          serviceImpl.return_((proto.HomeworkCNP.Cnp) request,
              (io.grpc.stub.StreamObserver<proto.HomeworkCNP.CnpReply>) responseObserver);
          break;
        case METHODID_SAY_INFO_AGAIN:
          serviceImpl.sayInfoAgain((proto.HomeworkCNP.NameCNP) request,
              (io.grpc.stub.StreamObserver<proto.HomeworkCNP.ServerReply>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class ClientBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    ClientBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return proto.HomeworkCNP.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("Client");
    }
  }

  private static final class ClientFileDescriptorSupplier
      extends ClientBaseDescriptorSupplier {
    ClientFileDescriptorSupplier() {}
  }

  private static final class ClientMethodDescriptorSupplier
      extends ClientBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    ClientMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (ClientGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new ClientFileDescriptorSupplier())
              .addMethod(getSayInfoMethod())
              .addMethod(getReturnMethod())
              .addMethod(getSayInfoAgainMethod())
              .build();
        }
      }
    }
    return result;
  }
}
